import pytest

from main import myrange


def test_stop10():
    '''Полооожительный сценарий'''
    assert list(myrange(10)) == list(range(10))


def test_start2_stop10():
    '''Полооожительный сценарий'''
    assert list(myrange(2, 10)) == list(range(2, 10))


def test_start_negative2_stop10():
    '''Полооожительный сценарий'''
    assert list(myrange(-2, 10)) == list(range(-2, 10))


def test_start_stop_step():
    '''Полооожительный сценарий'''
    assert list(myrange(2, 10, 2)) == list(range(2, 10, 2))


def test_start_negative2_stop10_step2():
    '''Полооожительный сценарий'''
    assert list(myrange(-2, 10, 2)) == list(range(-2, 10, 2))


def test_start2_stop_negative10_step_negative2():
    '''Полооожительный сценарий'''
    assert list(myrange(2, -10, -2)) == list(range(2, -10, -2))


def test_start_negative2_stop_negative10_step_negative2():
    '''Полооожительный сценарий'''
    assert list(myrange(-2, -10, -2)) == list(range(-2, -10, -2))


def test_stop_negative10():
    '''Негативный сценарий'''
    assert list(myrange(-10)) == list(range(-10))


def test_start10_stop10():
    '''Негативный сценарий'''
    assert list(myrange(10, 10)) == list(range(10, 10))


def test_start2_stop_negative10():
    '''Негативный сценарий'''
    assert list(myrange(2, -10)) == list(range(2, -10))


def test_start_negative2_stop_negative10():
    '''Негативный сценарий'''
    assert list(myrange(-2, -10)) == list(range(-2, -10))


def test_start2_stop2_step2():
    '''Негативный сценарий'''
    assert list(myrange(2, 2, 2)) == list(range(2, 2, 2))


def test_start2_stop_negative10_step2():
    '''Негативный сценарий'''
    assert list(myrange(2, -10, 2)) == list(range(2, -10, 2))


def test_start_negative2_stop_negative10_step2():
    '''Негативный сценарий'''
    assert list(myrange(-2, -10, 2)) == list(range(-2, -10, 2))


def test_start2_stop10_step_negative2():
    '''Негативный сценарий'''
    assert list(myrange(2, 10, -2)) == list(range(2, 10, -2))


def test_start_negative2_stop10_step_negative2():
    '''Негативный сценарий'''
    assert list(myrange(-2, 10, -2)) == list(range(-2, 10, -2))


def test_step0():
    '''Негативный сценарий'''
    with pytest.raises(ValueError) as e:
        list(myrange(2, 10, 0))
        assert str(e.value) == "Step must not be zero"


def test_start_not_integer():
    '''Негативный сценарий'''
    with pytest.raises(TypeError) as e:
        list(myrange("start", 10, 2))
        assert str(e.value) == "Start, stop and step must be integer"


def test_stop_not_integer():
    '''Негативный сценарий'''
    with pytest.raises(TypeError) as e:
        list(myrange(2, "stop", 2))
    assert str(e.value) == "Start, stop and step must be integer"


def test_step_not_integer():
    '''Негативный сценарий'''
    with pytest.raises(TypeError) as e:
        list(myrange(2, 10, "step"))
    assert str(e.value) == "Start, stop and step must be integer"
