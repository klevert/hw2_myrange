from typing import List


def myrange(start: int, stop: int = None, step: int = 1) -> List[int]:
    '''
    The function return a list of integers from start (inclusive) to stop (exclusive) by step
    :param start: int
    :param stop: int
    :param step: int
    :return list of integers
    '''
    if step == 0:
        raise ValueError("Step must not be zero")

    if stop is None:
        stop = start
        start = 0

    if not isinstance(stop, int) or not isinstance(start, int) or not isinstance(step, int):
        raise TypeError("Start, stop and step must be integer")

    if step > 0 and start < stop:
        while start < stop:
            yield start
            start += step
    elif step < 0 and start > stop:
        while start > stop:
            yield start
            start += step
